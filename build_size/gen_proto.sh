#!/bin/bash

# Copyright 2022 The ChromiumOS Authors
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

protoc -I /mnt/host/source/infra/proto/src/ \
  --python_out \
  /mnt/host/source/infra/build/observability-cloud/build_size/cloud_functions/ \
  --proto_path /mnt/host/source/infra/proto/src/ \
  /mnt/host/source/infra/proto/src/chromite/observability/sizes.proto \
  /mnt/host/source/infra/proto/src/chromite/observability/shared.proto \
  /mnt/host/source/infra/proto/src/chromiumos/common.proto \
  /mnt/host/source/infra/proto/src/chromiumos/builder_config.proto
