# Copyright 2022 The ChromiumOS Authors
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

"""Release process for the build size cloud functions."""

import datetime
import logging
from pathlib import Path
import shutil
import sys
import tempfile

import pytest


# Make sure chromite is in the python path.
chromite_path = list(Path(__file__).absolute().parents)[4]
sys.path.insert(0, str(chromite_path.absolute()))

from chromite.lib import cipd
from chromite.lib import commandline
from chromite.lib import constants
from chromite.lib import cros_build_lib
from chromite.lib import git
from chromite.lib import osutils


# Proto version in cloud_functions/requirements.txt
PROTOC_VERSION = "3.17.0"
_CIPD_PACKAGE = "infra/tools/protoc/linux-amd64"
_CIPD_PACKAGE_VERSION = f"protobuf_version:v{PROTOC_VERSION}"


class Error(Exception):
    """Base error class for the module."""


class GenerationError(Error):
    """A failure we can't recover from."""


def install_protoc() -> Path:
    """Install protoc from CIPD."""
    cipd_root = Path(
        cipd.InstallPackage(
            cipd.GetCIPDFromCache(), _CIPD_PACKAGE, _CIPD_PACKAGE_VERSION
        )
    )
    return cipd_root / "protoc"


def _GenerateFiles(
    source: Path,
    output: Path,
    protoc_bin_path: Path,
):
    """Generate the proto files from the |source| tree into |output|.

    Args:
        source: Path to the proto source root directory.
        output: Path to the output root directory.
        protoc_bin_path: The protoc command to use.
    """
    logging.info("Generating files to %s.", output)

    targets = []

    chromeos_config_path = Path(constants.SOURCE_ROOT) / "src" / "config"

    with tempfile.TemporaryDirectory() as tempdir:
        # Clone the config proto into tempdir if needed.
        if not chromeos_config_path.exists():
            chromeos_config_path = Path(tempdir) / "config"

            logging.info("Creating shallow clone of chromiumos/config")
            git.Clone(
                chromeos_config_path,
                "%s/chromiumos/config" % constants.EXTERNAL_GOB_URL,
                depth=1,
            )

        for src_dir in (source, chromeos_config_path):
            targets.extend(list(src_dir.rglob("*.proto")))

        cmd = [
            protoc_bin_path,
            "-I",
            chromeos_config_path / "proto",
            "--python_out",
            output,
            "--proto_path",
            source,
        ]
        cmd.extend(targets)

        result = cros_build_lib.dbg_run(
            cmd,
            cwd=source,
            check=False,
        )
        # tempdir definitely no longer needed now that we've compiled the proto.

    if result.returncode:
        raise GenerationError(
            "Error compiling the proto. See the output for a " "message."
        )


def _InstallMissingInits(directory: Path):
    """Add any __init__.py files not present in the generated protobuf folders."""
    logging.info("Adding missing __init__.py files in %s.", directory)
    # glob ** returns only directories.
    for current in directory.rglob("**"):
        (current / "__init__.py").touch()


def copy_source_files(output: Path):
    """Copy the source files."""
    build_size = Path(__file__).parent.absolute()
    cloud_functions = build_size / "cloud_functions"

    targets = (
        "main.py",
        "requirements.txt",
        "chromite",
        "chromiumos",
        "sql",
    )

    for target in targets:
        file = cloud_functions / target
        output_target = output / target
        if file.is_dir():
            shutil.copytree(
                file,
                output_target,
                ignore=lambda x, fs: [f for f in fs if f == "__pycache__"],
            )
        else:
            shutil.copy(file, output_target)


def compile_proto(output: Path):
    """Compile the protobuf files.

    Args:
        output: The output directory.
    """
    source = Path(constants.SOURCE_ROOT) / "infra" / "proto" / "src"
    protoc_bin_path = install_protoc()
    _GenerateFiles(source, output, protoc_bin_path)
    _InstallMissingInits(output)


def create_release_archive(dest: Path, cwd: Path):
    """Create the release archive."""
    cros_build_lib.run(["zip", "-r", dest, "."], cwd=cwd)


def GetParser():
    """Build the argument parser."""
    parser = commandline.ArgumentParser(description=__doc__)
    parser.add_argument(
        "--destination",
        type="path",
        help="Directory where the release should be produced.",
    )
    return parser


def _ParseArguments(argv):
    """Parse and validate arguments."""
    parser = GetParser()
    opts = parser.parse_args(argv)

    opts.destination = Path(opts.destination or ".")

    opts.Freeze()
    return opts


def main(argv):
    opts = _ParseArguments(argv)

    now = datetime.datetime.utcnow()
    dt = now.strftime("%Y%m%d_%H%M%S")
    filename = f"cloud_functions_{dt}.zip"

    with osutils.TempDir(delete=False) as t:
        tempdir = Path(t)
        # compile_proto(tempdir)
        copy_source_files(tempdir)

        create_release_archive(opts.destination.absolute() / filename, tempdir)


if __name__ == "__main__":
    main(sys.argv[1:])
