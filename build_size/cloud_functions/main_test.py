# Copyright 2022 The ChromiumOS Authors
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

"""Tests for main.py."""

from typing import TYPE_CHECKING

from chromiumos import builder_config_pb2
from chromiumos import common_pb2
import main

from chromite.observability import sizes_pb2


if TYPE_CHECKING:
    from chromite.observability import shared_pb2


def _get_test_msg() -> sizes_pb2.ImageSizeObservabilityData:
    data = {
        "builder_metadata": {
            "buildbucket_id": 123456,
            "start_timestamp": {
                "seconds": 1600000000,
            },
            "build_target": {
                "name": "amd64-generic",
            },
            "build_type": builder_config_pb2.BuilderConfig.Id.POSTSUBMIT,
            "build_config_name": "amd64-generic-postsubmit",
            "annealing_commit_id": 54321,
            "manifest_commit": "deadbeef",
        },
        "build_version_data": {
            "milestone": 100,
            "platform_version": {
                "platform_build": 12345,
                "platform_branch": 6,
                "platform_patch": 7,
            },
        },
        "image_data": [
            {
                "image_type": common_pb2.IMAGE_TYPE_BASE,
                "image_partition_data": [
                    {
                        "partition_name": "rootfs",
                        "packages": [
                            {
                                "identifier": {
                                    "package_name": {
                                        "atom": "foo/bar",
                                        "category": "foo",
                                        "package_name": "bar",
                                    },
                                    "package_version": {
                                        "major": 1,
                                        "minor": 2,
                                        "patch": 3,
                                        "extended": 4,
                                        "revision": 5,
                                        "full_version": "1.2.3.4-r5",
                                    },
                                },
                                "apparent_size": 1,
                                "disk_utilization_size": 4096,
                            },
                            {
                                "identifier": {
                                    "package_name": {
                                        "atom": "cat/pkg",
                                        "category": "cat",
                                        "package_name": "pkg",
                                    },
                                    "package_version": {
                                        "major": 10,
                                        "revision": 11,
                                        "full_version": "10-r11",
                                    },
                                },
                                "apparent_size": 1,
                                "disk_utilization_size": 4096,
                            },
                            {
                                "identifier": {
                                    "package_name": {
                                        "atom": "virtual/target",
                                        "category": "virtual",
                                        "package_name": "target",
                                    },
                                    "package_version": {
                                        "patch": 1,
                                        "revision": 9999,
                                        "full_version": "0.0.1-r9999",
                                    },
                                },
                                "apparent_size": 0,
                                "disk_utilization_size": 0,
                            },
                        ],
                        "partition_apparent_size": 2,
                        "partition_disk_utilization_size": 8192,
                    },
                ],
            },
        ],
    }
    return sizes_pb2.ImageSizeObservabilityData(**data)


def test_parse_build_size_proto():
    """Quick deserialization verification."""
    msg = _get_test_msg()
    parsed = main.parse_build_size_proto(msg.SerializeToString())
    assert msg == parsed


def _assert_platform_version_equal(
    pv_msg: "shared_pb2.PlatformVersion", platform_version: main.PlatformVersion
):
    assert pv_msg.platform_build == platform_version.build
    assert pv_msg.platform_branch == platform_version.branch
    assert pv_msg.platform_patch == platform_version.patch


def test_platform_version_parse():
    """Verify platform version construction."""
    msg = _get_test_msg()
    pv_msg = msg.build_version_data.platform_version
    platform_version = main.PlatformVersion.from_proto(pv_msg)

    _assert_platform_version_equal(pv_msg, platform_version)


def _assert_build_target_equal(
    bt_msg: common_pb2.BuildTarget, build_target: main.BuildTarget
):
    assert build_target.name == bt_msg.name


def test_build_target_parse():
    """Verify build target construction."""
    msg = _get_test_msg()
    bt_msg = msg.builder_metadata.build_target
    build_target = main.BuildTarget.from_proto(bt_msg)

    _assert_build_target_equal(bt_msg, build_target)


def _assert_build_config_equal(
    config_msg: "shared_pb2.BuilderMetadata", config: main.BuildConfig
):
    assert config.build_config_name == config_msg.build_config_name
    assert config.build_type is main.BuildType.POSTSUBMIT


def test_build_config_parse():
    """Verify BuildConfig construction."""
    msg = _get_test_msg()
    config_msg = msg.builder_metadata
    config = main.BuildConfig.from_proto(config_msg)

    _assert_build_config_equal(config_msg, config)


def _assert_builder_metadata_equal(
    msg: sizes_pb2.ImageSizeObservabilityData,
    builder_metadata: main.BuilderMetadata,
):
    assert builder_metadata.build_id == msg.builder_metadata.buildbucket_id
    assert (
        builder_metadata.start_timestamp
        == msg.builder_metadata.start_timestamp.seconds
    )
    assert builder_metadata.milestone == msg.build_version_data.milestone
    assert (
        builder_metadata.annealing_commit_id
        == msg.builder_metadata.annealing_commit_id
    )
    assert (
        builder_metadata.manifest_commit == msg.builder_metadata.manifest_commit
    )


def test_builder_metadata_parse():
    """Verify BuilderMetadata construction."""
    msg = _get_test_msg()
    builder_metadata = main.BuilderMetadata.from_proto(msg)

    _assert_builder_metadata_equal(msg, builder_metadata)


def _assert_package_name_equal(
    name_msg: sizes_pb2.PackageName, pkg_name: main.PackageName
):
    assert name_msg.atom == pkg_name.atom
    assert name_msg.category == pkg_name.category
    assert name_msg.package_name == pkg_name.name


def test_package_name_parse():
    """Verify PackageName construction."""
    msg = _get_test_msg()
    pkgs = msg.image_data[0].image_partition_data[0].packages
    for pkg in pkgs:
        msg_name = pkg.identifier.package_name
        pkg_name = main.PackageName.from_proto(msg_name)

        _assert_package_name_equal(msg_name, pkg_name)


def _assert_package_version_equal(
    version_msg: sizes_pb2.PackageVersion, pkg_version: main.PackageVersion
):
    assert version_msg.major == pkg_version.major
    assert version_msg.minor == pkg_version.minor
    assert version_msg.patch == pkg_version.patch
    assert version_msg.extended == pkg_version.extended
    assert version_msg.revision == pkg_version.revision
    assert version_msg.full_version == pkg_version.full_version


def test_package_version_parse():
    """Verify PackageVersion construction."""
    msg = _get_test_msg()
    pkgs = msg.image_data[0].image_partition_data[0].packages
    for pkg in pkgs:
        msg_version = pkg.identifier.package_version
        pkg_version = main.PackageVersion.from_proto(msg_version)

        _assert_package_version_equal(msg_version, pkg_version)


def _assert_package_identifier_equal(
    identifier_msg: sizes_pb2.PackageIdentifier,
    pkg_identifier: main.PackageIdentifier,
):
    _assert_package_name_equal(
        identifier_msg.package_name, pkg_identifier.pkg_name
    )
    _assert_package_version_equal(
        identifier_msg.package_version, pkg_identifier.pkg_version
    )


def test_package_identifier_parse():
    """Verify PackageIdentifier construction."""
    msg = _get_test_msg()
    pkgs = msg.image_data[0].image_partition_data[0].packages
    for pkg in pkgs:
        msg_identifier = pkg.identifier
        pkg_identifier = main.PackageIdentifier.from_proto(msg_identifier)

        _assert_package_identifier_equal(msg_identifier, pkg_identifier)


def _assert_package_size_equal(
    pkg_msg: sizes_pb2.PackageSizeData, pkg_size: main.PackageSize
):
    _assert_package_identifier_equal(
        pkg_msg.identifier, pkg_size.package_identifier
    )
    assert pkg_msg.apparent_size == pkg_size.apparent_size
    assert pkg_msg.disk_utilization_size == pkg_size.disk_utilization_size


def test_package_size_parse():
    """Verify PackageSize construction."""
    msg = _get_test_msg()
    pkgs = msg.image_data[0].image_partition_data[0].packages
    for pkg in pkgs:
        pkg_size = main.PackageSize.from_proto(pkg)

        _assert_package_size_equal(pkg, pkg_size)


def _assert_image_partition_equal(
    partition_msg: sizes_pb2.ImagePartitionData,
    img_partition: main.ImagePartition,
):
    assert img_partition.name == partition_msg.partition_name
    assert img_partition.apparent_size == partition_msg.partition_apparent_size
    assert (
        img_partition.disk_utilization_size
        == partition_msg.partition_disk_utilization_size
    )
    assert len(list(img_partition.packages)) == len(partition_msg.packages)
    for pkg, pkg_size in zip(partition_msg.packages, img_partition.packages):
        _assert_package_size_equal(pkg, pkg_size)


def test_image_partition_parse():
    """Verify ImagePartition construction."""
    msg = _get_test_msg()
    partitions = msg.image_data[0].image_partition_data
    for partition in partitions:
        img_partition = main.ImagePartition.from_proto(partition)

        _assert_image_partition_equal(partition, img_partition)


def _assert_image_equal(image_msg: sizes_pb2.ImageData, image):
    assert image.image_type is main.ImageType.BASE
    assert len(image_msg.image_partition_data) == len(list(image.partitions))
    img_iter = zip(image_msg.image_partition_data, image.partitions)
    for img_partition, partition in img_iter:
        _assert_image_partition_equal(img_partition, partition)


def test_image_parse():
    """Verify Image construction."""
    msg = _get_test_msg()
    images = msg.image_data
    for img_msg in images:
        image = main.Image.from_proto(img_msg)

        _assert_image_equal(img_msg, image)


def test_build_size_parse():
    """Verify BuildSize construction."""
    msg = _get_test_msg()
    build_size = main.BuildSize.from_proto(msg)

    _assert_builder_metadata_equal(msg, build_size.builder_data)

    assert len(msg.image_data) == len(list(build_size.images))
    for img_msg, img in zip(msg.image_data, build_size.images):
        _assert_image_equal(img_msg, img)
