CREATE TABLE IF NOT EXISTS db_versions
(
    version_number INTEGER PRIMARY KEY,
    applied        TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE IF NOT EXISTS db_version_lock
(
    version_number INTEGER PRIMARY KEY,
    updater_uuid   CHAR(36) NOT NULL
);

CREATE TABLE IF NOT EXISTS packages
(
    package_id       SERIAL PRIMARY KEY,
    package_atom     VARCHAR(127) NOT NULL UNIQUE,
    package_name     VARCHAR(63)  NOT NULL,
    package_category VARCHAR(63)  NOT NULL
);

CREATE INDEX IF NOT EXISTS idx_packages__package_name
    ON packages (package_name);

CREATE TABLE IF NOT EXISTS package_versions
(
    package_version_id BIGSERIAL PRIMARY KEY,
    package_id         INTEGER      NOT NULL REFERENCES packages,
    major              INTEGER      NOT NULL DEFAULT 0,
    minor              INTEGER      NOT NULL DEFAULT 0,
    patch              INTEGER      NOT NULL DEFAULT 0,
    extended           INTEGER      NOT NULL DEFAULT 0,
    revision           INTEGER      NOT NULL DEFAULT 0,
    version            VARCHAR(127) NOT NULL,
    CONSTRAINT idx_package_versions__pkg_version_uniq
        UNIQUE (package_id, version)
);

CREATE INDEX IF NOT EXISTS idx_package_versions__sort_cmp
    ON package_versions (major, minor, patch, extended, revision, version);

CREATE TABLE IF NOT EXISTS build_configs
(
    build_config_id   BIGSERIAL PRIMARY KEY,
    build_target      VARCHAR(63)  NOT NULL,
    builder_type      VARCHAR(63)  NOT NULL,
    build_config_name VARCHAR(255) NOT NULL,
    CONSTRAINT idx_build_configs__target_type_name_uniq
        UNIQUE (build_target, builder_type, build_config_name)
);

CREATE INDEX IF NOT EXISTS idx_build_configs__build_config
    ON build_configs (build_config_name);
CREATE INDEX IF NOT EXISTS idx_build_configs__type
    ON build_configs (builder_type);

CREATE TABLE IF NOT EXISTS builds
(
    build_id            BIGINT UNIQUE PRIMARY KEY,
    build_config_id     BIGINT    NOT NULL REFERENCES build_configs,
    start_ts            TIMESTAMP NOT NULL,
    milestone           INTEGER,
    platform_build      INTEGER,
    platform_branch     INTEGER,
    platform_patch      INTEGER,
    annealing_commit_id INTEGER,
    manifest_commit     CHAR(40)
);

CREATE INDEX IF NOT EXISTS idx_builds__annealing_commit
    ON builds (annealing_commit_id);
CREATE INDEX IF NOT EXISTS idx_builds__milestone
    ON builds (milestone);
CREATE INDEX IF NOT EXISTS idx_builds__platform_version
    ON builds (platform_build, platform_branch, platform_patch);
CREATE INDEX IF NOT EXISTS idx_builds__start
    ON builds (start_ts);

CREATE TABLE IF NOT EXISTS images
(
    image_id   BIGSERIAL PRIMARY KEY,
    build_id   BIGINT      NOT NULL REFERENCES builds,
    image_type VARCHAR(31) NOT NULL,
    CONSTRAINT idx_images__build_type_uniq UNIQUE (build_id, image_type)
);

CREATE TABLE IF NOT EXISTS image_partition_sizes
(
    image_partition_size_id       BIGSERIAL PRIMARY KEY,
    image_id                      BIGINT      NOT NULL REFERENCES images,
    image_partition_name          VARCHAR(31) NOT NULL,
    image_partition_apparent_size BIGINT      NOT NULL,
    image_partition_disk_size     BIGINT      NOT NULL,
    CONSTRAINT idx_image_partition_sizes__image_partition_uniq
        UNIQUE (image_id, image_partition_name)
);

CREATE INDEX IF NOT EXISTS idx_image_partition_sizes__partition_name
    ON image_partition_sizes (image_partition_name);

CREATE TABLE IF NOT EXISTS package_sizes
(
    package_size_id         BIGSERIAL PRIMARY KEY,
    package_version_id      BIGINT NOT NULL REFERENCES package_versions,
    image_partition_size_id BIGINT NOT NULL REFERENCES image_partition_sizes,
    package_apparent_size   BIGINT NOT NULL,
    package_disk_size       BIGINT NOT NULL,
    CONSTRAINT idx_package_sizes__package_version_per_partition_uniq
        UNIQUE (image_partition_size_id, package_version_id)
);

CREATE TABLE IF NOT EXISTS image_partitions
(
    image_partition_id   SERIAL      PRIMARY KEY,
    image_type           VARCHAR(31) NOT NULL,
    image_partition_name VARCHAR(31) NOT NULL,
    CONSTRAINT idx_image_partition__type_partition_name_uniq
        UNIQUE (image_type, image_partition_name)
);

CREATE TABLE IF NOT EXISTS milestone_image_partition_sizes
(
    build_config_id     BIGINT  NOT NULL REFERENCES build_configs,
    image_partition_id  INTEGER NOT NULL REFERENCES image_partitions,
    milestone           INTEGER NOT NULL,
    min_apparent_size   BIGINT  NOT NULL,
    max_apparent_size   BIGINT  NOT NULL,
    final_apparent_size BIGINT  NOT NULL,
    min_disk_size       BIGINT  NOT NULL,
    max_disk_size       BIGINT  NOT NULL,
    final_disk_size     BIGINT  NOT NULL,
    CONSTRAINT idx_milestone_image_partition_sizes__bld_ms_img_partition_uniq
        UNIQUE (build_config_id, image_partition_id, milestone)
);

CREATE TABLE IF NOT EXISTS platform_image_partition_sizes
(
    build_config_id     BIGINT  NOT NULL REFERENCES build_configs,
    image_partition_id  INTEGER NOT NULL REFERENCES image_partitions,
    platform_build      INTEGER NOT NULL,
    platform_branch     INTEGER NOT NULL,
    platform_patch      INTEGER NOT NULL,
    min_apparent_size   BIGINT  NOT NULL,
    max_apparent_size   BIGINT  NOT NULL,
    final_apparent_size BIGINT  NOT NULL,
    min_disk_size       BIGINT  NOT NULL,
    max_disk_size       BIGINT  NOT NULL,
    final_disk_size     BIGINT  NOT NULL,
    CONSTRAINT idx_platform_image_sizes__build_pv_img_partition_uniq
        UNIQUE (build_config_id, image_partition_id, platform_build,
                platform_branch, platform_patch)
);

CREATE TABLE IF NOT EXISTS milestone_package_sizes
(
    build_config_id     BIGINT  NOT NULL REFERENCES build_configs,
    image_partition_id  INTEGER NOT NULL REFERENCES image_partitions,
    package_id          BIGINT  NOT NULL REFERENCES packages,
    milestone           INTEGER NOT NULL,
    min_apparent_size   BIGINT  NOT NULL,
    max_apparent_size   BIGINT  NOT NULL,
    final_apparent_size BIGINT  NOT NULL,
    min_disk_size       BIGINT  NOT NULL,
    max_disk_size       BIGINT  NOT NULL,
    final_disk_size     BIGINT  NOT NULL,
    CONSTRAINT idx_milestone_package_sizes__build_ms_img_partition_pkg_uniq
        UNIQUE (build_config_id, milestone, image_partition_id, package_id)
);

CREATE TABLE IF NOT EXISTS platform_package_sizes
(
    build_config_id     BIGINT  NOT NULL REFERENCES build_configs,
    image_partition_id  INTEGER NOT NULL REFERENCES image_partitions,
    package_id          BIGINT  NOT NULL REFERENCES packages,
    platform_build      INTEGER NOT NULL,
    platform_branch     INTEGER NOT NULL,
    platform_patch      INTEGER NOT NULL,
    min_apparent_size   BIGINT  NOT NULL,
    max_apparent_size   BIGINT  NOT NULL,
    final_apparent_size BIGINT  NOT NULL,
    min_disk_size       BIGINT  NOT NULL,
    max_disk_size       BIGINT  NOT NULL,
    final_disk_size     BIGINT  NOT NULL,
    CONSTRAINT idx_platform_package_sizes__build_pv_img_partition_pkg_uniq
        UNIQUE (build_config_id, platform_build, platform_branch,
                platform_patch, image_partition_id, package_id)
);

CREATE TABLE IF NOT EXISTS package_version_sizes
(
    build_config_id     BIGINT  NOT NULL REFERENCES build_configs,
    image_partition_id  INTEGER NOT NULL REFERENCES image_partitions,
    package_version_id  BIGINT  NOT NULL REFERENCES package_versions,
    min_apparent_size   BIGINT  NOT NULL,
    max_apparent_size   BIGINT  NOT NULL,
    final_apparent_size BIGINT  NOT NULL,
    min_disk_size       BIGINT  NOT NULL,
    max_disk_size       BIGINT  NOT NULL,
    final_disk_size     BIGINT  NOT NULL,
    CONSTRAINT idx_package_version_sizes__build_img_partition_pkg_ver_uniq
        UNIQUE (build_config_id, image_partition_id, package_version_id)
);

CREATE OR REPLACE VIEW base_image_rootfs_sizes AS
SELECT
    images.build_id                   AS build_id,
    ips.image_partition_apparent_size AS rootfs_apparent_size,
    ips.image_partition_disk_size     AS rootfs_disk_size
FROM
    images
    INNER JOIN image_partition_sizes ips ON images.image_id = ips.image_id
WHERE
    images.image_type = 'base'
    AND ips.image_partition_name = 'rootfs';

CREATE OR REPLACE VIEW base_image_rootfs_package_sizes AS
SELECT
    i.build_id               AS build_id,
    p.package_atom           AS package_atom,
    p.package_name           AS package_name,
    pv.major                 AS package_version_major,
    pv.minor                 AS package_version_minor,
    pv.patch                 AS package_version_patch,
    pv.extended              AS package_version_extended,
    pv.revision              AS package_version_revision,
    pv.version               AS package_version_full,
    ps.package_apparent_size AS package_apparent_size,
    ps.package_disk_size     AS package_disk_size
FROM
    images i
    INNER JOIN image_partition_sizes ips
        ON ips.image_id = i.image_id
    INNER JOIN package_sizes ps
        ON ps.image_partition_size_id = ips.image_partition_size_id
    INNER JOIN package_versions pv
        ON pv.package_version_id = ps.package_version_id
    INNER JOIN packages p
        ON p.package_id = pv.package_id
WHERE
    i.image_type = 'base'
    AND ips.image_partition_name = 'rootfs';

INSERT INTO db_versions (version_number)
VALUES (1);
