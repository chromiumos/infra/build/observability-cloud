# Copyright 2022 The ChromiumOS Authors
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

"""Cloud function entry point implementations."""

import base64
import contextlib
import csv
import datetime
import enum
import io
import logging
import os
from pathlib import Path
import sys
import time
from typing import Any, Dict, Iterable, NamedTuple, Optional
import uuid

from chromiumos import builder_config_pb2
from chromiumos import common_pb2
from google.cloud import secretmanager
import google_crc32c
import sqlalchemy

from chromite.observability import sizes_pb2


_PROJECT = "cros-build-telemetry"
_DB = {}


class Error(Exception):
    """Module level base error."""


class DatabaseVersionCollisionError(Error):
    """Multiple files for a single db version."""


class DataCorruptionError(Error):
    """Data corruption detected."""


class MissingRowError(Error):
    """Missing row that was expected to exist."""


@enum.unique
class BuildSizeDbSecrets(enum.Enum):
    """Manages the names of the secrets for the different environments."""

    # The different environments.
    PROD = enum.auto()
    STAGING = enum.auto()

    # The base secret names.
    # Host: localhost, 1.2.3.4
    _HOST = "build-size-db-host"
    # User: The DB user.
    _USER = "build-size-db-user"
    # Pass: The DB user's password.
    _PASS = "build-size-db-pass"
    # DB: The database in the instance to use.
    _DB = "build-size-db-db"
    # Connection Name: <project>:<region>:<db>. Used for socket connection.
    _CONNECTION_NAME = "build-size-db-connection-name"

    def _modify_key(self, key):
        """Modify the key for the environment."""
        return f"staging-{key}" if self is self.STAGING else key

    @property
    def host(self):
        """Get the database host."""
        return _get_secret(self._modify_key("build-size-db-host"))

    @property
    def user(self):
        """Get the database user."""
        return _get_secret(self._modify_key("build-size-db-user"))

    @property
    def password(self):
        """Get the database user's password."""
        return _get_secret(self._modify_key("build-size-db-pass"))

    @property
    def db(self):
        """Get the database."""
        return _get_secret(self._modify_key("build-size-db-db"))

    @property
    def connection_name(self):
        """Get the connection name."""
        return _get_secret(self._modify_key("build-size-db-connection-name"))


class Database(object):
    """Class to manage the db connection."""

    _pool: sqlalchemy.engine.Engine = None

    def __init__(self):
        self._conn = None

    @property
    def engine(self) -> sqlalchemy.engine.Engine:
        """Singleton db access."""
        if not self._pool:
            self._pool = self._get_engine()
            self._update_database()

        return self._pool

    def execute(self, query, *args, **kwargs) -> sqlalchemy.engine.CursorResult:
        """Execute a query, returning the cursor result."""
        stmt = sqlalchemy.text(query)
        if kwargs:
            stmt = stmt.bindparams(*args, **kwargs)

        if self._conn:
            return self._conn.execute(stmt)
        else:
            return self.engine.execute(stmt)

    def get(self, query: str, default: Any = None, **kwargs) -> Any:
        """Get the value from a query that produces a single value."""
        result = self.execute(query, **kwargs)
        if not result.rowcount:
            return default
        else:
            return list(dict(result.first()).values()).pop()

    def copy(self, tmp_create, copy, insert, stream):
        """Run a COPY FROM STDIN query."""
        if self._conn:
            conn = self._conn.raw_connection()
        else:
            conn = self.engine.raw_connection(self._conn)
        cursor = conn.cursor()  # type: pg8000.dbapi.Cursor

        cursor.execute("START TRANSACTION")
        cursor.execute(tmp_create)
        cursor.execute(copy, stream=stream)
        cursor.execute(insert)
        cursor.execute("COMMIT")

    @contextlib.contextmanager
    def begin(self):
        """Transaction context manager."""
        with self.engine.begin() as conn:
            self._conn = conn
            yield
            self._conn = None

    def _update_database(self) -> None:
        """Update the database if necessary."""
        # Get latest in DB.
        latest_version_stmt = "SELECT MAX(version_number) FROM db_versions"
        db_version = self.get(latest_version_stmt, default=0)

        # Get latest sql file.
        sqldir = Path(__file__).absolute().parent / "sql"
        latest = 0
        files = {}
        for f in sqldir.iterdir():
            num_component = f.name.split("_")[0]
            try:
                version = int(num_component)
            except ValueError:
                logging.warning(f"Skipping {f}")
                continue

            latest = max(latest, version)
            if version in files:
                raise DatabaseVersionCollisionError(
                    f"Multiple files exist for {version}: {files[version]}, {f}"
                )
            files[version] = f

        # DB is up-to-date.
        if db_version >= latest:
            logging.debug("Database up to date.")
            return

        logging.info("Updating database to version %s", latest)
        # Try to insert self as updater.
        my_id = str(uuid.uuid4())
        stmt = (
            "INSERT INTO db_version_lock (version_number, updater_uuid) "
            "VALUES (:version, :uid) ON CONFLICT DO NOTHING"
        )
        self.execute(stmt, version=latest, uid=my_id)

        # Check if we're updating the DB.
        stmt = (
            "SELECT updater_uuid FROM db_version_lock "
            "WHERE version_number = :version"
        )
        updater_uuid = self.get(stmt, version=latest)
        logging.debug("My UUID: %s, %s", my_id, type(my_id))
        logging.debug("Updater UUID: %s, %s", updater_uuid, type(updater_uuid))
        if updater_uuid != my_id:
            logging.info("Waiting for another process to update DB.")
            # Wait for it to be updated by some other function.
            # TODO: Handle the updater failing.
            while db_version != latest:
                time.sleep(2)
                db_version = self.get(latest_version_stmt, default=0)

            return

        # Update the DB.
        for v in range(db_version + 1, latest + 1):
            logging.info("Executing version %s file", v)
            with open(files[v], "r") as f:
                self.execute(f.read())
        logging.info("Database update complete.")

    def _get_engine(self) -> sqlalchemy.engine.Engine:
        """Instantiate DB connection."""
        # Base DB config.
        db_config = {
            "client_encoding": "utf8",
            "max_overflow": 0,  # Allowed connection overage.
            "pool_recycle": 1800,  # 30 minutes
            "pool_size": 5,  # Max number of connections in pool.
            "pool_timeout": 30,  # 30 seconds
        }

        # Fetch DB info. Start with environment to allow simple local testing,
        # fetching from Cloud Secret Manager when not available.
        username = os.environ.get("PGUSER")
        password = os.environ.get("PGPASSWORD")
        database = os.environ.get("PGDATABASE")
        instance_conn_name = os.environ.get("INSTANCE_CONNECTION_NAME")
        socket_dir = os.environ.get("DB_SOCKET_DIR", "/cloudsql")
        socket_path = Path(f"{socket_dir}/{instance_conn_name}")
        host = query = None
        if not instance_conn_name or not socket_path.exists():
            host = os.environ.get("PGHOST")
            if host.startswith(os.sep):
                socket_path = Path(host)

        if socket_path.exists():
            query = {"unix_sock": str(socket_path / ".s.PGSQL.5432")}
            host = None

        pool = sqlalchemy.create_engine(
            sqlalchemy.engine.URL.create(
                drivername="postgresql+pg8000",
                host=host,
                username=username,
                password=password,
                database=database,
                query=query,
            ),
            **db_config,
        )

        return pool


def _get_db() -> Database:
    """Database wrapper (and connection) singleton getter."""
    global _DB
    if not _DB:
        _DB = Database()

    return _DB


def _get_secret(
    secret: str, version: str = "latest", verify: bool = True
) -> Optional[str]:
    """Get a secret from the secret manager."""
    try:
        client = secretmanager.SecretManagerServiceClient()
    except ValueError as e:
        logging.error(e)
        raise
    name = f"projects/{_PROJECT}/secrets/{secret}/versions/{version}"
    response = client.access_secret_version(request={"name": name})

    if verify:
        crc32c = google_crc32c.Checksum()
        crc32c.update(response.payload.data)
        if response.payload.data_crc32c != int(crc32c.hexdigest(), 16):
            raise DataCorruptionError("Data corruption detected.")

    return response.payload.data.decode("UTF-8")


class PlatformVersion(NamedTuple):
    """Data class for the platform version."""

    build: int
    branch: int
    patch: int

    @classmethod
    def from_proto(
        cls, message: "google.protobuf.message.Message"
    ) -> "PlatformVersion":
        """Build a PlatformVersion object from a proto message."""
        return cls(
            message.platform_build,
            message.platform_branch,
            message.platform_patch,
        )


class BuildTarget(NamedTuple):
    """Build target data."""

    name: str

    @classmethod
    def from_proto(
        cls, message: "google.protobuf.message.Message"
    ) -> "BuildTarget":
        """Build a BuildTarget object from a proto message."""
        return cls(name=message.name)


class BuildType(enum.Enum):
    """Build type enum."""

    CQ = builder_config_pb2.BuilderConfig.Id.CQ
    POSTSUBMIT = builder_config_pb2.BuilderConfig.Id.POSTSUBMIT
    RELEASE = builder_config_pb2.BuilderConfig.Id.RELEASE

    @property
    def db_name(self):
        return self.name.lower()


class BuildConfig(NamedTuple):
    """Build config data."""

    build_target: BuildTarget
    build_config_name: str
    build_type: BuildType

    @classmethod
    def from_proto(
        cls, message: "google.protobuf.message.Message"
    ) -> "BuildConfig":
        """Build a BuildConfig object from a proto message."""
        return cls(
            build_target=BuildTarget.from_proto(message.build_target),
            build_config_name=message.build_config_name,
            build_type=BuildType(message.build_type),
        )

    def insert(self, db: Database) -> int:
        """Save the build config data to the db."""
        insert = """
INSERT INTO build_configs (build_target, builder_type, build_config_name)
VALUES (
    :build_target,
    :builder_type,
    :build_config_name
)
ON CONFLICT DO NOTHING
"""
        db.execute(
            insert,
            build_target=self.build_target.name,
            builder_type=self.build_type.db_name,
            build_config_name=self.build_config_name,
        )

        select = """
SELECT build_config_id
FROM build_configs
WHERE build_target = :build_target
  AND builder_type = :builder_type
  AND build_config_name = :build_config_name
"""
        config_id = db.get(
            select,
            build_target=self.build_target.name,
            builder_type=self.build_type.db_name,
            build_config_name=self.build_config_name,
        )
        if not config_id:
            raise MissingRowError(
                f"Missing build config for: {self.build_target.name}, "
                f"{self.build_type}, {self.build_config_name}"
            )
        return int(config_id)


class BuilderMetadata(NamedTuple):
    """Data class for the builder metadata."""

    build_id: int
    start_timestamp: int
    build_config: BuildConfig
    milestone: int
    platform_version: PlatformVersion
    annealing_commit_id: int
    manifest_commit: str

    @classmethod
    def from_proto(
        cls, message: "google.protobuf.message.Message"
    ) -> "BuilderMetadata":
        """Build a BuilderMetadata object from a proto message."""
        return cls(
            build_id=message.builder_metadata.buildbucket_id,
            start_timestamp=message.builder_metadata.start_timestamp.seconds,
            build_config=BuildConfig.from_proto(message.builder_metadata),
            milestone=message.build_version_data.milestone,
            platform_version=PlatformVersion.from_proto(
                message.build_version_data.platform_version
            ),
            annealing_commit_id=message.builder_metadata.annealing_commit_id,
            manifest_commit=message.builder_metadata.manifest_commit,
        )

    def insert(self, db: Database, build_config_id: int) -> int:
        """Save the builder data to the db."""
        insert = """
INSERT INTO builds
(
    build_id,
    build_config_id,
    start_ts,
    milestone,
    platform_build,
    platform_branch,
    platform_patch,
    annealing_commit_id,
    manifest_commit
) VALUES (
    :build_id,
    :build_config_id,
    :start_ts,
    :milestone,
    :platform_build,
    :platform_branch,
    :platform_patch,
    :annealing_commit_id,
    :manifest_commit
)
ON CONFLICT DO NOTHING
"""
        db.execute(
            insert,
            build_id=self.build_id,
            build_config_id=build_config_id,
            start_ts=datetime.datetime.fromtimestamp(
                self.start_timestamp
            ).strftime("%Y-%m-%d %H:%M:%S"),
            milestone=self.milestone,
            platform_build=self.platform_version.build,
            platform_branch=self.platform_version.branch,
            platform_patch=self.platform_version.patch,
            annealing_commit_id=self.annealing_commit_id,
            manifest_commit=self.manifest_commit,
        )

        return self.build_id


class ImageType(enum.Enum):
    """Image types."""

    BASE = common_pb2.IMAGE_TYPE_BASE
    DEV = common_pb2.IMAGE_TYPE_DEV
    TEST = common_pb2.IMAGE_TYPE_TEST
    FACTORY = common_pb2.IMAGE_TYPE_FACTORY

    @property
    def db_name(self) -> str:
        return self.name.lower()


class PackageName(NamedTuple):
    """Package atom information."""

    atom: str
    category: str
    name: str

    @classmethod
    def from_proto(
        cls, message: "google.protobuf.message.Message"
    ) -> "PackageName":
        return cls(
            atom=message.atom,
            category=message.category,
            name=message.package_name,
        )


class PackageVersion(NamedTuple):
    """Package version information."""

    full_version: str
    major: int
    minor: int
    patch: int
    extended: int
    revision: int

    @classmethod
    def from_proto(
        cls, message: "google.protobuf.message.Message"
    ) -> "PackageVersion":
        return cls(
            full_version=message.full_version,
            major=message.major,
            minor=message.minor,
            patch=message.patch,
            extended=message.extended,
            revision=message.revision,
        )


class PackageIdentifier(NamedTuple):
    """PackageIdentifier data."""

    pkg_name: PackageName
    pkg_version: PackageVersion

    @classmethod
    def from_proto(
        cls, message: "google.protobuf.message.Message"
    ) -> "PackageIdentifier":
        return cls(
            pkg_name=PackageName.from_proto(message.package_name),
            pkg_version=PackageVersion.from_proto(message.package_version),
        )


class PackageSize(NamedTuple):
    """Package size data."""

    package_identifier: PackageIdentifier
    apparent_size: int
    disk_utilization_size: int

    @classmethod
    def from_proto(
        cls, message: "google.protobuf.message.Message"
    ) -> "PackageSize":
        return cls(
            package_identifier=PackageIdentifier.from_proto(message.identifier),
            apparent_size=message.apparent_size,
            disk_utilization_size=message.disk_utilization_size,
        )


class ImagePartition(NamedTuple):
    """Image partition data."""

    name: str
    apparent_size: int
    disk_utilization_size: int
    packages: Iterable[PackageSize]

    @classmethod
    def from_proto(
        cls, message: "google.protobuf.message.Message"
    ) -> "ImagePartition":
        """Build ImagePartition object from a proto message."""
        return cls(
            name=message.partition_name,
            apparent_size=message.partition_apparent_size,
            disk_utilization_size=message.partition_disk_utilization_size,
            packages=[PackageSize.from_proto(x) for x in message.packages],
        )

    def insert(self, db: Database, image_id: int) -> int:
        """Save the image partition data to the db."""
        insert = """
INSERT INTO image_partition_sizes (
    image_id,
    image_partition_name,
    image_partition_apparent_size,
    image_partition_disk_size
) VALUES (
    :image_id,
    :partition_name,
    :partition_apparent_size,
    :partition_disk_size
)
ON CONFLICT DO NOTHING
"""
        db.execute(
            insert,
            image_id=image_id,
            partition_name=self.name,
            partition_apparent_size=self.apparent_size,
            partition_disk_size=self.disk_utilization_size,
        )

        select = """
SELECT image_partition_size_id
FROM image_partition_sizes
WHERE image_id = :image_id
  AND image_partition_name = :partition_name
"""
        partition_id = db.get(
            select, image_id=image_id, partition_name=self.name
        )
        if not partition_id:
            raise MissingRowError(f"Missing partition: {image_id}, {self.name}")

        return int(partition_id)


class Image(NamedTuple):
    """An image from a build."""

    image_type: ImageType
    partitions: Iterable[ImagePartition]

    @classmethod
    def from_proto(cls, message: "google.protobuf.message.Message") -> "Image":
        """Build Image object from a proto message."""
        return cls(
            image_type=ImageType(message.image_type),
            partitions=[
                ImagePartition.from_proto(x)
                for x in message.image_partition_data
            ],
        )

    def insert(self, db: Database, build_id: int) -> int:
        """Save the image data to the db."""
        insert = """
INSERT INTO images (build_id, image_type)
VALUES (:build_id, :image_type)
ON CONFLICT DO NOTHING
"""
        db.execute(
            insert, build_id=build_id, image_type=self.image_type.db_name
        )

        select = """
SELECT image_id
FROM images
WHERE build_id = :build_id
  AND image_type = :image_type
"""
        img_id = db.get(
            select,
            build_id=build_id,
            image_type=self.image_type.db_name,
        )

        if not img_id:
            raise MissingRowError(
                f"Missing image: {build_id}, {self.image_type.db_name}"
            )

        return int(img_id)


class BuildSize(NamedTuple):
    """Data class for all build size data."""

    builder_data: BuilderMetadata
    images: Iterable[Image]

    @classmethod
    def from_proto(
        cls, message: "google.protobuf.message.Message"
    ) -> "BuildSize":
        """Build BuildSize object from a proto message."""
        return cls(
            builder_data=BuilderMetadata.from_proto(message),
            images=[Image.from_proto(x) for x in message.image_data],
        )

    def insert(self, db: Database):
        """Save all the build size data to the db."""
        # Save build and build config data.
        build_config_id = self.builder_data.build_config.insert(db)
        build_id = self.builder_data.insert(db, build_config_id)

        # Save the images and their partitions.
        pkgs = set()
        partition_pkgs = {}
        for image in self.images:
            image_id = image.insert(db, build_id)
            for partition in image.partitions:
                partition_id = partition.insert(db, image_id)
                pkgs.update(
                    set([x.package_identifier for x in partition.packages])
                )
                partition_pkgs[partition_id] = partition.packages

        logging.debug("Saving %d packages.", len(pkgs))
        # Save the packages, package versions, and package sizes.
        pkgs_stream = self._write_pkgs(pkgs)
        self._save_pkgs(db, pkgs_stream)
        pkg_ids = self._get_pkg_ids(db, pkgs)

        versions_stream = self._write_pkg_versions(pkg_ids, pkgs)
        self._save_pkg_versions(db, versions_stream)
        pkg_version_ids = self._get_pkg_version_ids(db, pkgs, pkg_ids)

        sizes_stream = self._write_pkg_sizes(partition_pkgs, pkg_version_ids)
        self._save_pkg_sizes(db, sizes_stream)

    def _write_pkgs(self, pkgs: Iterable[PackageIdentifier]) -> io.StringIO:
        """Write the packages as a CSV to a stream for insertion."""
        stream = io.StringIO()
        pkg_writer = csv.writer(stream)
        seen = set()
        for pkg in pkgs:
            if pkg.pkg_name.atom in seen:
                continue
            pkg_writer.writerow(
                (
                    pkg.pkg_name.atom,
                    pkg.pkg_name.name,
                    pkg.pkg_name.category,
                )
            )
            seen.add(pkg.pkg_name.atom)
        stream.seek(0)
        return stream

    def _write_pkg_versions(
        self,
        pkg_ids: Dict,
        pkgs: Iterable[PackageIdentifier],
    ) -> io.StringIO:
        """Write the package versions to a CSV for insertion."""
        stream = io.StringIO()
        writer = csv.writer(stream)
        for pkg in pkgs:
            writer.writerow(
                (
                    pkg_ids[pkg.pkg_name.atom],
                    pkg.pkg_version.major,
                    pkg.pkg_version.minor,
                    pkg.pkg_version.patch,
                    pkg.pkg_version.extended,
                    pkg.pkg_version.revision,
                    pkg.pkg_version.full_version,
                )
            )
        stream.seek(0)
        return stream

    def _write_pkg_sizes(
        self,
        partition_pkgs: Dict[int, Iterable[PackageSize]],
        pkg_version_ids: Dict,
    ) -> io.StringIO:
        stream = io.StringIO()
        writer = csv.writer(stream)
        for partition_id, pkgs in partition_pkgs.items():
            for pkg in pkgs:
                atom = pkg.package_identifier.pkg_name.atom
                version = pkg.package_identifier.pkg_version.full_version
                writer.writerow(
                    (
                        partition_id,
                        pkg_version_ids[atom][version],
                        pkg.apparent_size,
                        pkg.disk_utilization_size,
                    )
                )
        stream.seek(0)
        return stream

    def _save_pkgs(self, db: Database, stream: io.StringIO) -> None:
        """Save packages to the database."""
        tmp_create = (
            "CREATE TEMP TABLE tmp_pkgs (LIKE packages INCLUDING ALL) "
            "ON COMMIT DROP"
        )
        copy_pkgs = """
COPY tmp_pkgs (
    package_atom,
    package_name,
    package_category
)
FROM STDIN WITH (FORMAT CSV)
"""
        insert = """
INSERT INTO packages (
    package_atom,
    package_name,
    package_category
)
(SELECT package_atom, package_name, package_category FROM tmp_pkgs)
ON CONFLICT DO NOTHING
"""

        db.copy(tmp_create, copy_pkgs, insert, stream)

    def _save_pkg_versions(self, db: Database, stream: io.StringIO) -> None:
        tmp_create = (
            "CREATE TEMP TABLE tmp_pkg_versions "
            "(LIKE package_versions INCLUDING ALL) "
            "ON COMMIT DROP"
        )
        copy_vers = """
COPY tmp_pkg_versions (
    package_id,
    major,
    minor,
    patch,
    extended,
    revision,
    version
)
FROM STDIN WITH (FORMAT CSV)
"""
        insert = """
INSERT INTO package_versions (
    package_id,
    major,
    minor,
    patch,
    extended,
    revision,
    version
) (
SELECT
    package_id,
    major,
    minor,
    patch,
    extended,
    revision,
    version
FROM tmp_pkg_versions
)
ON CONFLICT DO NOTHING
"""

        db.copy(tmp_create, copy_vers, insert, stream)

    def _save_pkg_sizes(self, db: Database, stream: io.StringIO) -> None:
        tmp_create = (
            "CREATE TEMP TABLE tmp_pkg_sizes "
            "(LIKE package_sizes INCLUDING ALL) "
            "ON COMMIT DROP"
        )
        copy_sizes = """
COPY tmp_pkg_sizes (
    image_partition_size_id,
    package_version_id,
    package_apparent_size,
    package_disk_size
)
FROM STDIN WITH (FORMAT CSV)
"""
        insert = """
INSERT INTO
    package_sizes (image_partition_size_id,
                   package_version_id,
                   package_apparent_size,
                   package_disk_size)
    (SELECT
         image_partition_size_id,
         package_version_id,
         package_apparent_size,
         package_disk_size
     FROM
         tmp_pkg_sizes)
ON CONFLICT DO NOTHING
        """

        db.copy(tmp_create, copy_sizes, insert, stream)

    def _get_pkg_ids(
        self, db: Database, pkgs: Iterable[PackageIdentifier]
    ) -> Dict:
        atoms = {f"atom_{i}": x.pkg_name.atom for i, x in enumerate(pkgs)}
        binds = ",".join(f":{k}" for k in atoms.keys())
        pkg_select = f"""
SELECT package_id, package_atom
FROM packages
WHERE package_atom IN ({binds})
"""
        pkg_results = db.execute(pkg_select, **atoms)
        pkg_ids = {}
        for row in pkg_results:
            pkg_ids[row["package_atom"]] = int(row["package_id"])
            pkg_ids[int(row["package_id"])] = row["package_atom"]

        return pkg_ids

    def _get_pkg_version_ids(
        self, db: Database, pkgs: Iterable[PackageIdentifier], pkg_ids: Dict
    ) -> Dict:
        package_ids = {
            f"pkgid_{i}": pkg_ids[x.pkg_name.atom] for i, x in enumerate(pkgs)
        }
        binds = ",".join(f":{k}" for k in package_ids.keys())
        pkg_select = f"""
SELECT package_version_id, package_id, version
FROM package_versions
WHERE package_id IN ({binds})
"""
        pkg_results = db.execute(pkg_select, **package_ids)
        pkg_version_ids = {}
        for row in pkg_results:
            pkg_id = int(row["package_id"])
            pkg_version_id = int(row["package_version_id"])
            pkg_version = row["version"]
            pkg_atom = pkg_ids[pkg_id]
            # package_id: {version: package_version_id, ...}
            pkg_version_ids.setdefault(pkg_id, {})
            pkg_version_ids[pkg_id][pkg_version] = pkg_version_id
            # package_atom: {version: package_version_id, ...}
            pkg_version_ids.setdefault(pkg_atom, {})
            pkg_version_ids[pkg_atom][pkg_version] = pkg_version_id

        return pkg_version_ids


def parse_build_size_proto(proto_str: str) -> "google.protobuf.message.Message":
    """Parse a build size message from the wire/json format into a message."""
    message = sizes_pb2.ImageSizeObservabilityData()
    message.ParseFromString(proto_str)
    return message


def config_logging():
    """Configure the logger."""
    logging.basicConfig(level=logging.DEBUG, stream=sys.stdout)


def build_size_v1(event: dict, context: "google.cloud.functions.Context"):
    """Background Cloud Function to be triggered by Pub/Sub.

    Args:
        event: The `data` field maps to the PubsubMessage data in a
            base64-encoded string. The `attributes` field maps to the
            PubsubMessage attributes if any is present.
        context (google.cloud.functions.Context): Metadata of triggering event.
    """
    config_logging()
    raw_message = base64.b64decode(event["data"])
    proto_message = parse_build_size_proto(raw_message)
    build_size = BuildSize.from_proto(proto_message)
    build_size.insert(_get_db())
